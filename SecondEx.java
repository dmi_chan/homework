import java.util.Scanner;

public class SecondEx {

	public static void main(String[] args) {
		System.out.println("Enter the amount of money earned per hour");
		Scanner scan = new Scanner(System.in);
		int salary = scan.nextInt();
		System.out.println("Enter the number of working hours per week");
		int hours = scan.nextInt();
		int result = summa(salary, hours);
		System.out.println("Weekly wage = "+result);
	}
	
	public static int summa(int salary, int hours) {
		int result = 0;
		if (hours < 168) {
		result = hours * salary;
		}
		return result;
	}
}