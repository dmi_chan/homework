package net.codejava;

import java.util.Scanner;

public class ThirdEx {

	public static void main(String[] args) {
		System.out.println("Enter salary per hour and number of hours");
		Scanner scan = new Scanner(System.in);
		System.out.print("$");
		int salary = scan.nextInt();
		if (salary > 8) {
			int hours = scan.nextInt();
			if (hours <= 60) {
				if (hours > 40) {
					double newHours;
					newHours = 40 + (hours - 40) * 1.5;
					double result = salary * newHours;
					System.out.println(result);
				} else {
					int result = salary * hours;
					System.out.println(result);
				}
			} else
				System.out.println("An employee cannot work more than 60 hours a week");
		} else
			System.out.println("Salary per hour can't be less than $8");
	}
}