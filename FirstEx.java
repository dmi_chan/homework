import java.util.Scanner;

public class FirstEx {

	public static void main(String[] args) {
		System.out.println("Vvedite 2 chisla");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		int b = scan.nextInt();
		System.out.println("Enter operation");
		char sign = scan.next().charAt(0);
		int result = 0;
		switch (sign) {
		case '+':
			result = a + b;
			break;
		case '-':
			result = a - b;
			break;
		case '*':
			result = a * b;
			break;
		case '/':
			result = a / b;
			break;
		}
		System.out.println("Operation result = "+result);
	}

}